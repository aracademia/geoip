<?php
/**
 * User: rrafia
 * Date: 01/28/16
 */

namespace Aracademia\Geoip\Facades;


use Illuminate\Support\Facades\Facade;

class GeoipFacade extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'Geoip';
    }

} 