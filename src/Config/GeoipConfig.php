<?php
/**
 * Created by PhpStorm.
 * User: rrafia
 * Date: 1/28/2016
 * Time: 4:17 PM
 */

return [
    'geoip_db_path'                        => env('GEOIP_DB_PATH', app_path('Geoip/GeoIP2-Country.mmdb'))
];