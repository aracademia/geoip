<?php
/**
 * User: rrafia
 * Date: 01/28/16
 */

namespace Aracademia\Geoip;

use GeoIp2\Database\Reader;
use GeoIp2\WebService\Client;

class Geoip{

    protected $readerClass;
    public $ipAddress;
    protected $client;


    public function __construct()
    {
        $this->readerClass = new Reader(config('Geoip.geoip_db_path'));
        $this->client = new Client(51099,'fQ0C2zi6xEQt');
    }

    public function continentCode($ipAddress = null)
    {
        $ip = $this->ip($ipAddress);
        return $this->readerClass->country($ip)->continent->code;
    }

    public function countryIsoCode($ipAddress = null)
    {
        $ip = $this->ip($ipAddress);
        return $this->readerClass->country($ip)->country->isoCode;
    }

    public function countryName($ipAddress = null)
    {
        $ip = $this->ip($ipAddress);
        return $this->readerClass->country($ip)->country->name;
    }

    private function ip($ipAddress)
    {
        if(!is_null($ipAddress))
        {
            return $ipAddress;
        }
        if(!is_null($this->ipAddress))
        {
            return $this->ipAddress;
        }
        return $_SERVER['SERVER_ADDR'];
    }


}